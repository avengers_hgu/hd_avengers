
#include <iostream>

#include "Detector.h"

using namespace std;
using namespace cv;

HANDLE hMutex;

//Mat frame;
Mat marked(480, 640, CV_8UC1);
vector<Rect> found;
vector<Rect> found2;
vector<Rect> found3;

// output vector : rod (region of detected)
void Detect_haarcascades2(Mat &frame, vector<Rect> &rod, String CascadeName1, String CascadeName2, String CascadeName3)
{
	// detector (14x28 template)
	CascadeClassifier detector1;
	CascadeClassifier detector2;
	CascadeClassifier detector3;
	// Region of Detected

	if (!detector1.load(CascadeName1))
	{
		cerr << "ERROR: Could not load classifier cascade" << endl;
		return;
	}
	if (!detector2.load(CascadeName2))
	{
		cerr << "ERROR: Could not load classifier cascade" << endl;
		return;
	}
	if (!detector3.load(CascadeName3))
	{
		cerr << "ERROR: Could not load classifier cascade" << endl;
		return;
	}


	// parameters
	int gr_thr = 3;
	double scale_step1 = 1.05;
	double scale_step2 = 1.1;
	double scale_step3 = 1.1;

	Size min_obj_sz(48, 96);
	Size max_obj_sz(100, 200);

	// long distance vehicle
	Size min_obj_sz_step1(25, 25);
	Size max_obj_sz_step1(55, 55);

	Size min_obj_sz_step2(50, 50);
	Size max_obj_sz_step2(192, 192);

	Size min_obj_sz_step3(90, 90);
	Size max_obj_sz_step3(192, 192);

	Mat step1_Roi;
	Mat step2_Roi;
	Mat step3_Roi;

	// input image
	int w = frame.size().width;
	int h = frame.size().height;
	int x1, x2, x3, y1, y2, y3;
	int w1, w2, w3, h1, h2, h3;
	x1 = w / 3; y1 = h * 0.4; w1 = w / 3; h1 = h *0.22;
	x2 = w * 0.4; y2 = h * 0.38; w2 = w *0.6; h2 = h *0.5;
	x3 = w * 0.6; y3 = h * 0.3; w3 = w * 0.4; h3 = h * 0.7;
	Rect step1_Obj(x1, y1, w1, h1);
	step1_Roi = frame(step1_Obj);

	Rect step2_Obj(x2, y2, w2, h2);
	step2_Roi = frame(step2_Obj);

	Rect step3_Obj(x3, y3, w3, h3);
	step3_Roi = frame(step3_Obj);

	// Visualize ROI
	rectangle(frame, step1_Obj, Scalar(0, 255, 255), 2);
	rectangle(frame, step2_Obj, Scalar(255, 0, 255), 2);
	rectangle(frame, step3_Obj, Scalar(255, 255, 0), 2);

	// detect
	//detector.detectMultiScale(frame, found, scale_step, gr_thr, 0, min_obj_sz, max_obj_sz);
	detector1.detectMultiScale(step1_Roi, found, scale_step1, gr_thr, 0, min_obj_sz_step1, max_obj_sz_step1);
	detector2.detectMultiScale(step2_Roi, found2, scale_step2, gr_thr, 0, min_obj_sz_step2, max_obj_sz_step2);
	detector3.detectMultiScale(step3_Roi, found3, scale_step3, gr_thr, 0, min_obj_sz_step3, max_obj_sz_step3);


	//**************************
	// edit ROI for sub-window
	//**************************
	for (int i = 0; i < (int)found.size(); i++) {
		found[i].x += x1;
		found[i].y += y1;
	}
	for (int i = 0; i < (int)found2.size(); i++) {
		found2[i].x += x2;
		found2[i].y += y2;
	}
	for (int i = 0; i < (int)found3.size(); i++) {
		found3[i].x += x3;
		found3[i].y += y3;
	}


	rod.reserve(found.size() + found2.size() + found3.size());
	rod.insert(rod.end(), found.begin(), found.end());
	rod.insert(rod.end(), found2.begin(), found2.end());
	rod.insert(rod.end(), found3.begin(), found3.end());






	for (int i = 0; i < found.size(); i++)
		rectangle(frame, found[i], Scalar(0, 255, 255), -1, 8, 0);

	for (int i = 0; i < found2.size(); i++)
		rectangle(frame, found2[i], Scalar(255, 0, 255), -1, 8, 0);

	for (int i = 0; i < found3.size(); i++)
		rectangle(frame, found3[i], Scalar(255, 255, 0), -1, 8, 0);

	return;
}




#if 0

void Detect_haarcascades(VideoCapture *vc)
{
	Mat frame;
	// detector (14x28 template)
	string cascadeName = "./cascade_single.xml";
	//string cascadeName = "./YK_FL.xml";
	CascadeClassifier detector;
	if (!detector.load(cascadeName))
	{
		cerr << "ERROR: Could not load classifier cascade" << endl;
		return;
	}

	// parameters
	int gr_thr = 3;
	double scale_step1 = 1.1;
	double scale_step2 = 1.2;
	double scale_step3 = 1.4;


	Size min_obj_sz(48, 96);
	Size max_obj_sz(100, 200);

	// long distance vehicle
	Size min_obj_sz_step1(25, 20);
	Size max_obj_sz_step1(45, 36);

	Size min_obj_sz_step2(50, 50);
	Size max_obj_sz_step2(150, 150);

	Size min_obj_sz_step3(160, 120);
	Size max_obj_sz_step3(320, 240);

	// run

	Mat frame2(480, 640, CV_8UC3);

	Mat Cumm(480, 640, CV_8UC1);
	Mat Cumm_P(480, 640, CV_8UC1);
	Mat Binary(480, 640, CV_8UC1);
	Mat Cannyedge(480, 640, CV_8UC1);
	Mat CannyRGB(480, 640, CV_8UC3);
	Mat Contour(480, 640, CV_8UC3);
	double canny_th1 = 20.0;
	double canny_th2 = 50.0;
	double max_thresh = 255;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	__int64 freq, start, finish;
	::QueryPerformanceFrequency((_LARGE_INTEGER*)&freq);
	uchar *temp_ptr;
	uchar *temp_ptr2;
	Mat step1_Roi;
	Mat step2_Roi;
	Mat step3_Roi;
	int temp;
	int update_num = 0;
	int update_num2 = 0;
	int update_max = 30;
	int threshold = 100;

	/*
	thread variable
	*/
	unsigned int thIdx = 0;
	HANDLE hand[MAX_THREADS];
	pfs frameSet = (pfs)malloc(sizeof(frameSet) * 3);


	while (1)
	{
		thIdx = 0;

		// input image
		*vc >> frame;
		if (frame.empty()) break;
		int w = frame.size().width;
		int h = frame.size().height;
		frame.copyTo(frame2);

		Rect step1_Obj(w / 4, h * 0.45, w - w * 2 / 3, h *0.18);
		step1_Roi = frame(step1_Obj);

		Rect step2_Obj(w * 0.4, h * 2 / 5, w - w / 2, h * 1 / 3);
		step2_Roi = frame(step2_Obj);

		Rect step3_Obj(w / 2, h * 2 / 5, w - w / 2, h * 3 / 5);
		step3_Roi = frame(step3_Obj);


		rectangle(frame, step1_Obj, Scalar(0, 255, 255), 2);
		rectangle(frame, step2_Obj, Scalar(255, 0, 255), 2);
		rectangle(frame, step3_Obj, Scalar(255, 255, 0), 2);

		::QueryPerformanceCounter((_LARGE_INTEGER*)&start);

		// detect

		//detector.detectMultiScale(frame, found, scale_step, gr_thr, 0, min_obj_sz, max_obj_sz);
		detector.detectMultiScale(step1_Roi, found, scale_step1, gr_thr, 0, min_obj_sz_step1, max_obj_sz_step1);
		detector.detectMultiScale(step2_Roi, found2, scale_step2, gr_thr, 0, min_obj_sz_step2, max_obj_sz_step2);
		detector.detectMultiScale(step3_Roi, found3, scale_step3, gr_thr, 0, min_obj_sz_step3, max_obj_sz_step3);

		// processing time (fps)
		::QueryPerformanceCounter((_LARGE_INTEGER*)&finish);
		double fps = freq / double(finish - start + 1);
		char fps_str[20];
		sprintf_s(fps_str, 20, "FPS: %.1lf", fps);
		putText(frame, fps_str, Point(5, 35), FONT_HERSHEY_SIMPLEX, 1., Scalar(0, 255, 0), 2);

		// edit ROI for sub-window
		for (int i = 0; i < (int)found.size(); i++) {
			found[i].x += w / 4;
			found[i].y += h * 0.45;
		}
		for (int i = 0; i < (int)found2.size(); i++) {
			found2[i].x += w * 0.4;
			found2[i].y += h * 2 / 5;
		}
		for (int i = 0; i < (int)found3.size(); i++) {
			found3[i].x += w / 2;
			found3[i].y += h * 2 / 5;
		}


		if (thIdx == MAX_THREADS){
			cout << "thIdx is equal to MAX_THREADS" << endl;
			system("pause");
		}

		/* Create Thread (decaying) */
		hand[thIdx] = (HANDLE)_beginthreadex(NULL, 0, BasicThread, &marked, CREATE_SUSPENDED, 0);
		//CloseHandle(hand[thIdx]);

		//WaitForSingleObject(hand[thIdx], INFINITE);
		thIdx++;

		//for (int i = 0; i < 480; i++){
		//	for (int j = 0; j < 640; j++)
		//	{
		//		(marked).at<uchar>(i, j) = ((marked).at<uchar>(i, j) - 3 < 0) ? 0 : (marked).at<uchar>(i, j) - 3;
		//	}
		//}



		if (found.size() > 0){
			//	memcpy(&frameSet[0]._found, &found, sizeof(vector<Rect>));
			//	memcpy(&frameSet[0]._frame, &frame, sizeof(Mat));
			//	frameSet[0]._marked = &marked;
			//
			//	/* Create Thread (undecaying) */
			//	hand[thIdx++] = (HANDLE)_beginthreadex(NULL, 0, undecayThread1, &frameSet[0], 0, 0);
			//	CloseHandle(hand[thIdx++]);
		}

		if (found2.size() > 0){
			//frameSet[1].found = found2;
			//frameSet[1].found.assign(found2.begin(), found2.end());
			//frame.copyTo(frameSet[1]._frame);
			//frameSet[1]._frame = frame.clone();
			//memcpy(&frameSet[1]._found, &found, sizeof(vector<Rect>));
			//memcpy(&frameSet[1]._frame, &frame, sizeof(Mat));
			frameSet[1]._marked = &marked;

			/* Create Thread (undecaying) */
			hand[thIdx] = (HANDLE)_beginthreadex(NULL, 0, undecayThread2, &found2, CREATE_SUSPENDED, 0);
			//CloseHandle(hand[thIdx]);
			thIdx++;
		}



		//for (int i = 0; i < thIdx; i++)
		//	CloseHandle(hand[i]);


		for (int i = 0; i < (int)found3.size(); i++)
		{
			rectangle(frame, found3[i], Scalar(255, 255, 0), 1);
			for (int y = found3[i].y; y < found3[i].y + found3[i].height; y++)
			{
				temp_ptr = marked.ptr<uchar>(y);
				for (int x = found3[i].x; x < found3[i].x + found3[i].width; x++)
				{
					temp_ptr[x] = (temp_ptr[x] + 150 > 255) ? 255 : temp_ptr[x] + 150;
				}
			}
		}

		//get binary image
		for (int y = 0; y < h; y++)
		{
			temp_ptr = Binary.ptr<uchar>(y);
			temp_ptr2 = marked.ptr<uchar>(y);
			for (int x = 0; x < w; x++)
			{
				temp_ptr[x] = (temp_ptr2[x] > 151) ? 255 : 0;
			}
		}

		for (DWORD i = 0; i < thIdx; i++)
		{
			ResumeThread(hand[i]);
		}
		WaitForMultipleObjects(thIdx, hand, TRUE, INFINITE);

		for (DWORD i = 0; i < thIdx; i++)
		{
			CloseHandle(hand[i]);
		}

		Canny(Binary, Cannyedge, (int)canny_th1, (int)canny_th2, 3);
		cvtColor(Cannyedge, CannyRGB, CV_GRAY2RGB);

		Contour = Mat::zeros(h, w, CV_8UC1);
		findContours(Cannyedge, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
		vector<vector<Point> > contours_poly(contours.size());
		vector<Rect> boundRect(contours.size());
		vector<Point2f>center(contours.size());
		vector<float>radius(contours.size());


		for (int i = 0; i < contours.size(); i++)
		{
			approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
			boundRect[i] = boundingRect(Mat(contours_poly[i]));
			minEnclosingCircle((Mat)contours_poly[i], center[i], radius[i]);
		}


		for (int i = 0; i < contours.size(); i++)
		{
			rectangle(Contour, boundRect[i].tl(), boundRect[i].br(), (255, 255, 255), 2, 8, 0);
			rectangle(frame2, boundRect[i].tl(), boundRect[i].br(), (255, 255, 255), 2, 8, 0);

			//drawContours(Contour, contours, i, (25, 255, 255), 2, 8, hierarchy, 0, Point());
		}

#if 0
		for (int i = 0; i < (int)found.size(); i++)
		{
			rectangle(frame, found[i], Scalar(0, 255, 255), 1);


			for (int y = found[i].y; y < found[i].y + found[i].height; y++)

				for (int x = found[i].x; x < found[i].x + found[i].width; x++)
				{
					marked.at<uchar>(y, x) = (marked.at<uchar>(y, x) + 150) ? marked.at<uchar>(y, x) + 150 : 255;

				}

		}
		for (int i = 0; i < (int)found2.size(); i++)
		{
			rectangle(frame, found2[i], Scalar(255, 0, 255), 1);
			for (int y = found2[i].y; y < found2[i].y + found2[i].height; y++)
				for (int x = found2[i].x; x < found2[i].x + found2[i].width; x++)
				{
					marked.at<uchar>(y, x) = (marked.at<uchar>(y, x) + 150) ? marked.at<uchar>(y, x) + 150 : 255;
				}


		}
		for (int i = 0; i < (int)found3.size(); i++)
		{
			rectangle(frame, found3[i], Scalar(255, 255, 0), 1);
			for (int y = found3[i].y; y < found3[i].y + found3[i].height; y++)
				for (int x = found3[i].x; x < found3[i].x + found3[i].width; x++)
				{
					marked.at<uchar>(y, x) = (marked.at<uchar>(y, x) + 150) ? marked.at<uchar>(y, x) + 150 : 255;
				}


		}

#endif



		//for (DWORD i = 0; i<thIdx; i++){
		//	//GetExitCodeThread(hand[i], &test);
		//	CloseHandle(hand[i]);
		//}


		imshow("avengers", frame);
		imshow("marked", marked);
		imshow("baniry", Binary);
		//imshow("Edge", Cannyedge);
		imshow("contour", Contour);
		imshow("output", frame2);
		char ch = waitKey(10);
		if (ch == 27) break;            // ESC Key
		else if (ch == 32)               // SPACE Key
		{
			while ((ch = waitKey(10)) != 32 && ch != 27);
			if (ch == 27) break;
		}

		if (found2.size() > 0){
			//WaitForSingleObject(hand[1], INFINITE);
			//			CloseHandle(hand[1]);
		}

		//if (thIdx > 0)
		//WaitForSingleObject(hand[1], INFINITE);
		//WaitForMultipleObjects(thIdx, hand, TRUE, INFINITE);
		DWORD test = 0;
		//for (int i = 0; i < thIdx; i++){
		//	GetExitCodeThread(hand[i], &test);
		//	CloseHandle(hand[i]);
		//}

	}
	CloseHandle(hMutex);

	free(frameSet);
}

#endif

#if 0

DWORD AcquireMutex(HANDLE mutex)
{
	return WaitForSingleObject(mutex, INFINITE);
}

unsigned int __stdcall BasicThread(void* arg) {
	Mat *marked = (Mat*)arg;

	cout << "decaying..";
	AcquireMutex(hMutex);

	//for (int i = 0; i < 100; i++)
	//	cout << "Thread 1 running..  ";
	////decaying
	for (int i = 0; i < 480; i++){
		for (int j = 0; j < 640; j++)
		{
			(*marked).at<uchar>(i, j) = ((*marked).at<uchar>(i, j) - 3 < 0) ? 0 : (*marked).at<uchar>(i, j) - 3;
		}
	}
	ReleaseMutex(hMutex);

	//_endthreadex(0);
	return 0;
}

unsigned int __stdcall undecayThread1(void* arg) {
	pfs frameSet = (pfs)arg;
	uchar *temp_ptr;

	cout << "undecaying1..";
	AcquireMutex(hMutex);

	for (int i = 0; i < (int)frameSet->_found.size(); i++)
	{

		rectangle(frameSet->_frame, frameSet->_found[i], Scalar(0, 255, 255), 1);

		for (int y = frameSet->_found[i].y; y < frameSet->_found[i].y + frameSet->_found[i].height; y++)
		{
			temp_ptr = frameSet->_marked->ptr<uchar>(y);
			for (int x = frameSet->_found[i].x; x < frameSet->_found[i].x + frameSet->_found[i].width; x++)
			{
				temp_ptr[x] = (temp_ptr[x] + 150 > 255) ? 255 : temp_ptr[x] + 150;
			}
		}
	}

	ReleaseMutex(hMutex);


	return 0;
}


unsigned int __stdcall undecayThread2(void* arg) {
	//pfs frameSet = (pfs)arg;
	//uchar *temp_ptr;

	cout << "undecaying2..";
	AcquireMutex(hMutex);

	//cout << frameSet->_marked->size().width << " ";

	cout << "ROI 2 : " << found2.size() << " ";


	for (int i = 0; i < found2.size(); i++)
	{
		rectangle(frame, found2[i], Scalar(255, 0, 255), 1);

		for (int y = found2[i].y; y < found2[i].y + found2[i].height; y++)
		{
			//temp_ptr = frameSet->_marked->ptr<uchar>(y);
			for (int x = found2[i].x; x < found2[i].x + found2[i].width; x++)
			{
				//cout << i << " ";
				marked.ptr(y)[x] = marked.ptr(y)[x] + 150 > 255 ? 255 : marked.ptr(y)[x] + 150;
				//temp_ptr[x] = (temp_ptr[x] + 150 > 255) ? 255 : temp_ptr[x] + 150;
				//temp_ptr[x] = (temp_ptr[x] + 150 > 255) ? 255 : temp_ptr[x] + 150;
			}
		}
	}

	ReleaseMutex(hMutex);

	//_endthreadex(0);

	return 0;
}


unsigned int __stdcall undecayThread3(void* arg) {
	pfs frameSet = (pfs)arg;
	uchar *temp_ptr;

	cout << "undecaying3..";

	for (int i = 0; i < (int)frameSet->_found.size(); i++)
	{

		rectangle(frameSet->_frame, frameSet->_found[i], Scalar(0, 255, 255), 1);

		for (int y = frameSet->_found[i].y; y < frameSet->_found[i].y + frameSet->_found[i].height; y++)
		{
			temp_ptr = frameSet->_marked->ptr<uchar>(y);
			for (int x = frameSet->_found[i].x; x < frameSet->_found[i].x + frameSet->_found[i].width; x++)
			{
				temp_ptr[x] = (temp_ptr[x] + 150 > 255) ? 255 : temp_ptr[x] + 150;
			}
		}
	}

	return 0;
}

#endif

DWORD AcquireMutex(HANDLE mutex)
{
	return WaitForSingleObject(mutex, INFINITE);
}

unsigned int __stdcall BasicThread(void* arg) {
	return 0;
}

unsigned int __stdcall undecayThread1(void* arg) {
	return 0;
}


unsigned int __stdcall undecayThread2(void* arg) {
	return 0;
}


unsigned int __stdcall undecayThread3(void* arg) {
	return 0;
}
