#include <iostream>
#include <io.h>
#include "Tracker.h"

using namespace std;
using namespace cv;

//RNG rng(12345);

bool addRemovePt = true;


void onMouse(int event, int x, int y, int flags, void* param)
{
	CallbackParam *p = (CallbackParam *)param;

	if (event == CV_EVENT_LBUTTONDOWN)
	{
		p->pt1.x = x;
		p->pt1.y = y;
		p->pt2 = p->pt1;
		p->drag = true;
	}
	if (event == CV_EVENT_LBUTTONUP)
	{
		int w = x - p->pt1.x;
		int h = y - p->pt1.y;

		p->roi.x = p->pt1.x;
		p->roi.y = p->pt1.y;
		p->roi.width = w;
		p->roi.height = h;
		p->drag = false;

		if (w >= 10 && h >= 10)
		{
			p->updated = true;
		}
	}
	if (p->drag && event == CV_EVENT_MOUSEMOVE)
	{
		if (p->pt2.x != x || p->pt2.y != y)
		{
			Mat img = p->frame.clone();
			p->pt2.x = x;
			p->pt2.y = y;
			rectangle(img, p->pt1, p->pt2, Scalar(0, 255, 0), 1);
			imshow("LK Demo", img);
		}
	}
}

static void readDir(){
	_finddata_t fd;
	long mp4;
	int result = 1;
	int index = 1;
	mp4 = _findfirst(".\\*.mp4", &fd);

	if (mp4 == -1){
		cout << "There is no files." << endl;
		return;
	}
	cout << "< Video File (.mp4) >" << endl;
	while (result != -1){
		cout << index << "." << fd.name << endl;
		result = _findnext(mp4, &fd);
		index++;
	}
	_findclose(mp4);
	return;
}

#if 0

unsigned int __stdcall OpticalFlowThread(void* arg){
	_tParam *tParam = (_tParam *)arg;
	if (!tParam->vc->isOpened()){
		cout << "Could not initialize capturing...\n";
		return 0;
	}
	VideoCapture *vc = tParam->vc;
	TermCriteria termcrit(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03);
	Size subPixWinSize(10, 10), winSize(31, 31);

	const int MAX_COUNT = 500;
	
	Mat frame, gray, prevGray, image, drag, dragGray, rod, rodGray;
	vector<Point2f> points[2];
	vector<Rect> rodRect;
	Rect temp;

	int count = 0;
	bool once = true;
	bool adjust = true; // adjust roi coordinates to draw new features once a time.

	for (;;)
	{
		// image acquisition & target init
		mtx.lock();
		*tParam->vc >> frame;
		mtx.unlock();
		if (frame.empty()){
			cout << "< frame is empty >" << endl;
			break;
		}

		frame.copyTo(image);
		cvtColor(frame, gray, COLOR_BGR2GRAY);


		if (once)
		{
			cout << "< goodFeaturesToTrack Start >" << endl;
			if (tParam->detectedRoi[tParam->indexDetectedRoi].size() <= 0)	break;

			rodRect = (tParam->detectedRoi[tParam->indexDetectedRoi]);
			
			cout << "rodRect = *(tParam->detectedRoi)\n";
			rod = image(rodRect[0]);
			cout << "rod = image(rodRect[tParam->indexDetectedRoi])\n";
			cvtColor(rod, rodGray, COLOR_BGR2GRAY);
			//drag = image(param.roi);
			//cvtColor(drag, dragGray, COLOR_BGR2GRAY);
			goodFeaturesToTrack(rodGray, points[0], MAX_COUNT, 0.03, 10, Mat(), 3, 0, 0.04);
			cornerSubPix(rodGray, points[0], subPixWinSize, cvSize(-1, -1), termcrit);

			cout << "detected image size : " << rod.size().width << " * " << rod.size().height << endl;

			for (size_t i = 0; i < points[0].size(); i++){
				cout << "points[0][" << i << "].x : " << points[0][i].x << " "
					<< "points[0][" << i << "].y : " << points[0][i].y << endl;
			}
			//system("pause");
			once = false;
		}
		else if (!points[0].empty()){
			//cout << "points[0].size : " << points[0].size() << endl;
			//	cout << "points[0][0].x : " << points[0][0].x << " points[0][0].y : " << points[0][0].y << endl;
			//	//	cout << "points[1][0].x : " << points[1][0].x << " points[1][0].y : " << points[1][0].y << endl;
			//	cout << "points[0][1].x : " << points[0][1].x << " points[0][1].y : " << points[0][1].y << endl;
			//	cout << "points[0][2].x : " << points[0][2].x << " points[0][2].y : " << points[0][2].y << endl;
			//	cout << "points[0][" << points[0].size() - 1 << "].x : " << points[0][points[0].size() - 1].x
			//		<< " points[0][" << points[0].size() - 1 << "].y : " << points[0][points[0].size() - 1].y << endl;
			//	count++;

			vector<uchar> status;
			vector<float> err;
			if (prevGray.empty())
				gray.copyTo(prevGray);

			if (adjust){
				for (int i = 0; i < points[0].size(); i++){
					points[0][i].x += rodRect[0].x;
					points[0][i].y += rodRect[0].y;
				}
				adjust = false;
			}

			//cout << "calcOpticalFlowPytLK START" << endl;
			calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 0, 0.001);
			size_t i, k;
			int avg_x = 0;
			int avg_y = 0;
			for (i = k = 0; i < points[1].size(); i++)
			{
				//if (i < points[1].size() - 1){
				//	if (norm(points[1][i + 1] - points[1][i]) <= 5)
				//		continue;
				//}
				//if (!status[i])
				//	continue;

				if (points[1][i].x > image.size().width)
					continue;

				avg_x += points[1][i].x;
				avg_y += points[1][i].y;
				points[1][k++] = points[1][i];
				//cout << "points[1][" << points[1].size() - 1 << "].x : " << points[1][points[1].size() - 1].x
				//	<< " points[1][" << points[1].size() - 1 << "].y : " << points[1][points[1].size() - 1].y << endl;
				circle(image, points[1][i], 4, Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)), -1, 8, 0);
			}
			points[1].resize(k);
			//	cout << points[1].size() << " ";
			if (k != 0){
				avg_x /= k;
				avg_y /= k;
				rectangle(image, Rect(avg_x - 40, avg_y - 40, 80, 80), Scalar(255, 0, 0), 2);
			}
			//points[1].resize(k);
		}

		imshow("Tracker", image);

		char c = (char)waitKey(10);
		if (c == 27)
			break;

		std::swap(points[1], points[0]);
		cv::swap(prevGray, gray);
	}











#if 0

	for (;;)
	{
		// image acquisition & target init
		if (param.drag)
		{
			if (waitKey(10) == 27) break;		// ESC key
			continue;
		}

		//*cap >> frame;
		if (frame.empty())
			break;

		param.frame = frame;
		frame.copyTo(image);
		cvtColor(image, gray, COLOR_BGR2GRAY);



		if (addRemovePt && param.roi.x > 0)// && points[1].size() < (size_t)MAX_COUNT)
		{
			cout << "< goodFeaturesToTrack Re-Start >" << endl;
			drag = image(param.roi);
			cvtColor(drag, dragGray, COLOR_BGR2GRAY);
			goodFeaturesToTrack(dragGray, points[0], MAX_COUNT, 0.03, 10, Mat(), 3, 0, 0.04);
			cornerSubPix(dragGray, points[0], subPixWinSize, cvSize(-1, -1), termcrit);

			cout << "image size : " << image.size().width << " * " << image.size().height << endl;
			cout << "drag size : " << param.roi.width << " * " << param.roi.height << endl;
			cout << "drag coordinates : " << param.roi.x << ", " << param.roi.y << endl;
			for (size_t i = 0; i < points[0].size(); i++){
				cout << "points[0][" << i << "].x : " << points[0][i].x << " "
					<< "points[0][" << i << "].y : " << points[0][i].y << endl;
			}
			//system("pause");
			addRemovePt = false;
		}
		else if (!points[0].empty()){
			//cout << "points[0].size : " << points[0].size() << endl;
			//	cout << "points[0][0].x : " << points[0][0].x << " points[0][0].y : " << points[0][0].y << endl;
			//	//	cout << "points[1][0].x : " << points[1][0].x << " points[1][0].y : " << points[1][0].y << endl;
			//	cout << "points[0][1].x : " << points[0][1].x << " points[0][1].y : " << points[0][1].y << endl;
			//	cout << "points[0][2].x : " << points[0][2].x << " points[0][2].y : " << points[0][2].y << endl;
			//	cout << "points[0][" << points[0].size() - 1 << "].x : " << points[0][points[0].size() - 1].x
			//		<< " points[0][" << points[0].size() - 1 << "].y : " << points[0][points[0].size() - 1].y << endl;
			//	count++;

			vector<uchar> status;
			vector<float> err;
			if (prevGray.empty())
				gray.copyTo(prevGray);
			if (once){
				for (int i = 0; i < points[0].size(); i++){
					points[0][i].x += param.roi.x;
					points[0][i].y += param.roi.y;
				}
				once = 0;
			}
			//cout << "calcOpticalFlowPytLK START" << endl;
			calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 0, 0.001);
			size_t i, k;
			int avg_x = 0;
			int avg_y = 0;
			for (i = k = 0; i < points[1].size(); i++)
			{
				//if (i < points[1].size() - 1){
				//	if (norm(points[1][i + 1] - points[1][i]) <= 5)
				//		continue;
				//}
				//if (!status[i])
				//	continue;

				if (points[1][i].x > image.size().width)
					continue;

				avg_x += points[1][i].x;
				avg_y += points[1][i].y;
				points[1][k++] = points[1][i];
				//cout << "points[1][" << points[1].size() - 1 << "].x : " << points[1][points[1].size() - 1].x
				//	<< " points[1][" << points[1].size() - 1 << "].y : " << points[1][points[1].size() - 1].y << endl;
				circle(image, points[1][i], 4, Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)), -1, 8, 0);
			}
			points[1].resize(k);
			//	cout << points[1].size() << " ";
			if (k != 0){
				avg_x /= k;
				avg_y /= k;
				rectangle(image, Rect(avg_x - 40, avg_y - 40, 80, 80), Scalar(255, 0, 0), 2);
			}
			//points[1].resize(k);
		}

		imshow("LK Demo", image);

		char c = (char)waitKey(10);
		if (c == 27)
			break;

		std::swap(points[1], points[0]);
		cv::swap(prevGray, gray);
	}
#endif

	return 0;
}

int OpticalFlowThread2(_tParam *arg){
	_tParam *tParam = arg;
	cout << "thread~!" << endl;
	if (!tParam->vc->isOpened()){
		cout << "Could not initialize capturing...\n";
		return 0;
	}
	TermCriteria termcrit(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03);
	Size subPixWinSize(10, 10), winSize(31, 31);

	const int MAX_COUNT = 500;


	Mat frame, gray, prevGray, image, drag, dragGray, rod, rodGray;
	vector<Point2f> points[2];
	vector<Rect> rodRect;

	int count = 0;
	bool once = true;
	bool adjust = true; // adjust roi coordinates to draw new features once a time.

	for (;;)
	{
		// image acquisition & target init
		*tParam->vc >> frame;
		if (frame.empty()){
			cout << "frame is empty" << endl;
			break;
		}

	
		frame.copyTo(image);
		cvtColor(frame, gray, COLOR_BGR2GRAY);


		if (once)
		{
			cout << "< goodFeaturesToTrack Start >" << endl;
			if (tParam->detectedRoi[tParam->indexDetectedRoi].size() < 0)	break;

			rodRect = *(tParam->detectedRoi);
			cout << "rodRect = *(tParam->detectedRoi)\n";
			cout << "index DetectedRoi count : " << tParam->indexDetectedRoi << endl;
			cout << "x : " << rodRect[tParam->indexDetectedRoi].x << ", y : " << rodRect[tParam->indexDetectedRoi].y << endl;
			if (tParam->indexDetectedRoi < 0){
				cout << "indexDetectedRoi < 0\n";
				break;
			}
			rod = image(rodRect[tParam->indexDetectedRoi]);
			cout << "rod = image(rodRect[tParam->indexDetectedRoi])\n";
			cvtColor(rod, rodGray, COLOR_BGR2GRAY);
			//drag = image(param.roi);
			//cvtColor(drag, dragGray, COLOR_BGR2GRAY);
			goodFeaturesToTrack(rodGray, points[0], MAX_COUNT, 0.03, 10, Mat(), 3, 0, 0.04);
			cornerSubPix(rodGray, points[0], subPixWinSize, cvSize(-1, -1), termcrit);

			cout << "detected image size : " << rod.size().width << " * " << rod.size().height << endl;

			for (size_t i = 0; i < points[0].size(); i++){
				cout << "points[0][" << i << "].x : " << points[0][i].x << " "
					<< "points[0][" << i << "].y : " << points[0][i].y << endl;
			}
			//system("pause");
			once = false;
		}
		else if (!points[0].empty()){
			//cout << "points[0].size : " << points[0].size() << endl;
			//	cout << "points[0][0].x : " << points[0][0].x << " points[0][0].y : " << points[0][0].y << endl;
			//	//	cout << "points[1][0].x : " << points[1][0].x << " points[1][0].y : " << points[1][0].y << endl;
			//	cout << "points[0][1].x : " << points[0][1].x << " points[0][1].y : " << points[0][1].y << endl;
			//	cout << "points[0][2].x : " << points[0][2].x << " points[0][2].y : " << points[0][2].y << endl;
			//	cout << "points[0][" << points[0].size() - 1 << "].x : " << points[0][points[0].size() - 1].x
			//		<< " points[0][" << points[0].size() - 1 << "].y : " << points[0][points[0].size() - 1].y << endl;
			//	count++;

			vector<uchar> status;
			vector<float> err;
			if (prevGray.empty())
				gray.copyTo(prevGray);

			if (adjust){
				for (int i = 0; i < points[0].size(); i++){
					points[0][i].x += rodRect[tParam->indexDetectedRoi].x;
					points[0][i].y += rodRect[tParam->indexDetectedRoi].y;
				}
				adjust = false;
			}

			//cout << "calcOpticalFlowPytLK START" << endl;
			calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 0, 0.001);
			size_t i, k;
			int avg_x = 0;
			int avg_y = 0;
			for (i = k = 0; i < points[1].size(); i++)
			{
				//if (i < points[1].size() - 1){
				//	if (norm(points[1][i + 1] - points[1][i]) <= 5)
				//		continue;
				//}
				//if (!status[i])
				//	continue;

				if (points[1][i].x > image.size().width)
					continue;

				avg_x += points[1][i].x;
				avg_y += points[1][i].y;
				points[1][k++] = points[1][i];
				//cout << "points[1][" << points[1].size() - 1 << "].x : " << points[1][points[1].size() - 1].x
				//	<< " points[1][" << points[1].size() - 1 << "].y : " << points[1][points[1].size() - 1].y << endl;
				circle(image, points[1][i], 4, Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)), -1, 8, 0);
			}
			points[1].resize(k);
			//	cout << points[1].size() << " ";
			if (k != 0){
				avg_x /= k;
				avg_y /= k;
				rectangle(image, Rect(avg_x - 40, avg_y - 40, 80, 80), Scalar(255, 0, 0), 2);
			}
			//points[1].resize(k);
		}

		imshow("Tracker", image);

		char c = (char)waitKey(10);
		if (c == 27)
			break;

		std::swap(points[1], points[0]);
		cv::swap(prevGray, gray);
	}

	return 0;
}

int BSD_OpticalFlowLK(VideoCapture *vc, vector<Rect> detectedRoi){
	if (!vc->isOpened()){
		cout << "Could not initialize capturing...\n";
		return 0;
	}
	TermCriteria termcrit(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03);
	Size subPixWinSize(10, 10), winSize(31, 31);

	const int MAX_COUNT = 500;


	namedWindow("LK Demo", 1);

	CallbackParam param;
	param.drag = false;
	param.updated = false;
	setMouseCallback("LK Demo", onMouse, &param);

	Mat frame, gray, prevGray, image, drag, dragGray, rod, rodGray;
	vector<Point2f> points[2];

	int count = 0;
	bool once = true;
	bool adjust = true; // adjust roi coordinates to draw new features once a time.

	for (;;)
	{
		// image acquisition & target init
		if (param.drag)
		{
			if (waitKey(10) == 27) break;		// ESC key
			continue;
		}

		*vc >> frame;
		if (frame.empty())
			break;

		param.frame = frame;
		frame.copyTo(image);
		cvtColor(frame, gray, COLOR_BGR2GRAY);


		if (once)
		{
			cout << "< goodFeaturesToTrack Start >" << endl;
			if (detectedRoi.size() < 0) break;
			
			rod = image(detectedRoi[0]);
			cvtColor(rod, rodGray, COLOR_BGR2GRAY);
			//drag = image(param.roi);
			//cvtColor(drag, dragGray, COLOR_BGR2GRAY);
			goodFeaturesToTrack(rodGray, points[0], MAX_COUNT, 0.03, 10, Mat(), 3, 0, 0.04);
			cornerSubPix(rodGray, points[0], subPixWinSize, cvSize(-1, -1), termcrit);

			cout << "detected image size : " << rod.size().width << " * " << rod.size().height << endl;
			
			for (size_t i = 0; i < points[0].size(); i++){
				cout << "points[0][" << i << "].x : " << points[0][i].x << " "
					<< "points[0][" << i << "].y : " << points[0][i].y << endl;
			}
			//system("pause");
			once = false;
		}
		else if (!points[0].empty()){
			//cout << "points[0].size : " << points[0].size() << endl;
			//	cout << "points[0][0].x : " << points[0][0].x << " points[0][0].y : " << points[0][0].y << endl;
			//	//	cout << "points[1][0].x : " << points[1][0].x << " points[1][0].y : " << points[1][0].y << endl;
			//	cout << "points[0][1].x : " << points[0][1].x << " points[0][1].y : " << points[0][1].y << endl;
			//	cout << "points[0][2].x : " << points[0][2].x << " points[0][2].y : " << points[0][2].y << endl;
			//	cout << "points[0][" << points[0].size() - 1 << "].x : " << points[0][points[0].size() - 1].x
			//		<< " points[0][" << points[0].size() - 1 << "].y : " << points[0][points[0].size() - 1].y << endl;
			//	count++;

			vector<uchar> status;
			vector<float> err;
			if (prevGray.empty())
				gray.copyTo(prevGray);

			if (adjust){
				for (int i = 0; i < points[0].size(); i++){
					points[0][i].x += detectedRoi[0].x;
					points[0][i].y += detectedRoi[0].y;
				}
				adjust = false;
			}

			//cout << "calcOpticalFlowPytLK START" << endl;
			calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 0, 0.001);
			size_t i, k;
			int avg_x = 0;
			int avg_y = 0;
			for (i = k = 0; i < points[1].size(); i++)
			{
				//if (i < points[1].size() - 1){
				//	if (norm(points[1][i + 1] - points[1][i]) <= 5)
				//		continue;
				//}
				//if (!status[i])
				//	continue;

				if (points[1][i].x > image.size().width)
					continue;

				avg_x += points[1][i].x;
				avg_y += points[1][i].y;
				points[1][k++] = points[1][i];
				//cout << "points[1][" << points[1].size() - 1 << "].x : " << points[1][points[1].size() - 1].x
				//	<< " points[1][" << points[1].size() - 1 << "].y : " << points[1][points[1].size() - 1].y << endl;
				circle(image, points[1][i], 4, Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)), -1, 8, 0);
			}
			points[1].resize(k);
			//	cout << points[1].size() << " ";
			if (k != 0){
				avg_x /= k;
				avg_y /= k;
				rectangle(image, Rect(avg_x - 40, avg_y - 40, 80, 80), Scalar(255, 0, 0), 2);
			}
			//points[1].resize(k);
		}

		imshow("Tracker", image);

		char c = (char)waitKey(10);
		if (c == 27)
			break;

		std::swap(points[1], points[0]);
		cv::swap(prevGray, gray);
	}











#if 0

	for (;;)
	{
		// image acquisition & target init
		if (param.drag)
		{
			if (waitKey(10) == 27) break;		// ESC key
			continue;
		}

		//*cap >> frame;
		if (frame.empty())
			break;

		param.frame = frame;
		frame.copyTo(image);
		cvtColor(image, gray, COLOR_BGR2GRAY);

		

		if (addRemovePt && param.roi.x > 0)// && points[1].size() < (size_t)MAX_COUNT)
		{
			cout << "< goodFeaturesToTrack Re-Start >" << endl;
			drag = image(param.roi);
			cvtColor(drag, dragGray, COLOR_BGR2GRAY);
			goodFeaturesToTrack(dragGray, points[0], MAX_COUNT, 0.03, 10, Mat(), 3, 0, 0.04);
			cornerSubPix(dragGray, points[0], subPixWinSize, cvSize(-1, -1), termcrit);

			cout << "image size : " << image.size().width << " * " << image.size().height << endl;
			cout << "drag size : " << param.roi.width << " * " << param.roi.height << endl;
			cout << "drag coordinates : " << param.roi.x << ", " << param.roi.y << endl;
			for (size_t i = 0; i < points[0].size(); i++){
				cout << "points[0][" << i << "].x : " << points[0][i].x << " "
					<< "points[0][" << i << "].y : " << points[0][i].y << endl;
			}
			//system("pause");
			addRemovePt = false;
		}
		else if (!points[0].empty()){
			//cout << "points[0].size : " << points[0].size() << endl;
			//	cout << "points[0][0].x : " << points[0][0].x << " points[0][0].y : " << points[0][0].y << endl;
			//	//	cout << "points[1][0].x : " << points[1][0].x << " points[1][0].y : " << points[1][0].y << endl;
			//	cout << "points[0][1].x : " << points[0][1].x << " points[0][1].y : " << points[0][1].y << endl;
			//	cout << "points[0][2].x : " << points[0][2].x << " points[0][2].y : " << points[0][2].y << endl;
			//	cout << "points[0][" << points[0].size() - 1 << "].x : " << points[0][points[0].size() - 1].x
			//		<< " points[0][" << points[0].size() - 1 << "].y : " << points[0][points[0].size() - 1].y << endl;
			//	count++;

			vector<uchar> status;
			vector<float> err;
			if (prevGray.empty())
				gray.copyTo(prevGray);
			if (once){
				for (int i = 0; i < points[0].size(); i++){
					points[0][i].x += param.roi.x;
					points[0][i].y += param.roi.y;
				}
				once = 0;
			}
			//cout << "calcOpticalFlowPytLK START" << endl;
			calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 0, 0.001);
			size_t i, k;
			int avg_x = 0;
			int avg_y = 0;
			for (i = k = 0; i < points[1].size(); i++)
			{
				//if (i < points[1].size() - 1){
				//	if (norm(points[1][i + 1] - points[1][i]) <= 5)
				//		continue;
				//}
				//if (!status[i])
				//	continue;

				if (points[1][i].x > image.size().width)
					continue;

				avg_x += points[1][i].x;
				avg_y += points[1][i].y;
				points[1][k++] = points[1][i];
				//cout << "points[1][" << points[1].size() - 1 << "].x : " << points[1][points[1].size() - 1].x
				//	<< " points[1][" << points[1].size() - 1 << "].y : " << points[1][points[1].size() - 1].y << endl;
				circle(image, points[1][i], 4, Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)), -1, 8, 0);
			}
			points[1].resize(k);
		//	cout << points[1].size() << " ";
			if (k != 0){
				avg_x /= k;
				avg_y /= k;
				rectangle(image, Rect(avg_x - 40, avg_y - 40, 80, 80), Scalar(255, 0, 0), 2);
			}
			//points[1].resize(k);
		}

		imshow("LK Demo", image);

		char c = (char)waitKey(10);
		if (c == 27)
			break;

		std::swap(points[1], points[0]);
		cv::swap(prevGray, gray);
	}
#endif

	return 0;

}

#endif