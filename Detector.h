
#include <windows.h>
#include <Tchar.h>

#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "use_opencv.h"


typedef struct frameSet *pfs;
struct frameSet{
	Mat _frame;
	Mat *_marked;
	vector<Rect> _found;
};


void Detect_haarcascades(VideoCapture *vc);
// output vector : rod (region of detected)
void Detect_haarcascades2(cv::Mat &frame, vector<Rect> &rod, String CascadeName1, String CascadeName2, String CascadeName3 );

unsigned int __stdcall BasicThread(void* arg);
unsigned int __stdcall undecayThread1(void* arg);
unsigned int __stdcall undecayThread2(void* arg);
unsigned int __stdcall undecayThread3(void* arg);
DWORD AcquireMutex(HANDLE mutex);