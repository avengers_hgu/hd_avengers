#include "use_opencv.h"

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

#define MAX_TRACK 30


class Car {
	Rect m_aHistory[MAXHIST];
	int m_Tracking;
	int m_Penalty;      // Correctness between Detector and Tracker
	bool m_Verified = false;

public:
	Car();
	//Car(int nHistory);
	~Car() { /*Delete();*/ }

	//void Delete();
	//int Alloc(int nHistory);
	//int IsAllocated() { return m_aHistory != NULL; }
	int GetTracking() { return m_Tracking; }
	int GetPenalty() { return m_Penalty; }
	bool GetVerified() { return m_Verified; }
	Rect GetHistory(int index) { return m_aHistory[index]; }
	void UpdateTracking();
	void RenewTracking(int index);
	void UpdateNew(Rect newCar);
	void UpdateHistory(Rect newcar);
	void UpdateHistory(int index, Rect newcar);
	void UpdatePenalty();
	void UpdateVerified(bool tf);
	void Die();


};








#if 0
#include "use_opencv.h"

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif


class Car{
	Rect *m_aHistory;

	int m_Tracking;
	int m_Penalty;
	int m_Verified;

public:
	Car();
	Car(int nHistory);
	~Car(){ Delete(); }

	void Delete();
	int Alloc(int nHistory);
	int IsAllocated(){ return m_aHistory != NULL; }
	int IsTracking() { return m_Tracking != NULL; }
	int GetTracking() { return m_Tracking; }
	int GetPenalty() { return m_Penalty; }
	void UpdateTracking();
	void UpdatePenalty();
	Rect GetHistory(){ return *m_aHistory; }
};
#endif