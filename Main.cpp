#include <iostream>
#include <windows.h>
//#include <process.h>
//#include <thread>
#include <pthread.h>

//#include "Tracker.h"
#include "Detector.h"
#include "Car.h"


#define MAX_THREADS 3
#define MAXHIST 200
#define MAXTARGET 3
#define MAXPENALTY 1
#define NUMRECT 10

using namespace std;
using namespace cv;

int compare(vector<Rect> rect_in);

// using Tracker.cpp(mutex)
#if 1
struct ThreadInfo{
	VideoCapture *vc;
	vector<Rect> detectedRoi;
	int indexDetectedRoi;
	int _indexCar = 0; // index : pool of car
};
#endif


Mat frame;
Mat dFrame;	// detect Frame
Mat tFrame; // tracking Frame
ThreadInfo threadInfo;
RNG rng(12345);


unsigned int thIdx = 0;
void* killedThread;

Car car[MAXTARGET];

pthread_mutex_t mtx_readCar[MAXTARGET], mtx_wrtCar[MAXTARGET], mtx_wrtFrame, mtx_readFrame;
//pthread_mutex_t read, objElementRead, wrt;
//int readCount = 0;
//int writeCount = 0;
int frameReadCount = 0;
int frameWriteCount = 0;
int indexWriteCount = 0;
int carReadCount[MAXTARGET] = { 0 };
int carWriteCount[MAXTARGET] = { 0 };

// using Tracker.cpp(mutex)
//unsigned int __stdcall OpticalFlowThread(void* arg){
void* OpticalFlowThread(void* arg){
	//ThreadInfo *thInfo = (ThreadInfo *)arg;
	//if (!thInfo->vc->isOpened()){
	//	cout << "Could not initialize capturing...\n";
	//	return 0;
	//}

	int *indexCar = (int*)arg;
	cout << "indexCar : " << *indexCar << endl;

	//VideoCapture *vc = ThreadInfo->vc;
	TermCriteria termcrit(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03);
	Size subPixWinSize(10, 10), winSize(31, 31);
	const int MAX_COUNT = 500;
	//int indexCar = thInfo->_indexCar++;
	//int indexRod = thInfo->indexDetectedRoi;

	Mat  gray, prevGray, image, drag, dragGray, rod, rodGray;
	vector<Point2f> points[2];
	vector<Rect> rodRect;
	Rect temp;

	int count = 0;
	bool once = true;
	bool once_p = false;
	bool adjust = true; // adjust roi coordinates to draw new features once a time.
	//string index = to_string(indexRod);
	//string tracker = "Tracker";
	//tracker.append(index);

	string sindex = to_string(*indexCar);
	string tracker = "Tracker";
	tracker.append(sindex);

	bool carVerified = 0;
	int carTracking = 0;
	Rect carCurrPosition;

	for (;;)
	{
		// image acquisition & target init

		/* entry section */
		pthread_mutex_lock(&mtx_wrtCar[*indexCar]);
		carWriteCount[*indexCar]++;
		if (carWriteCount[*indexCar] > 1) {
			cout << "err : this is occupied by another writer\n";
			//	while (writeCount > 1);
		}

		/* critical section */
		carVerified = car[*indexCar].GetVerified();
		carTracking = car[*indexCar].GetTracking();
		carCurrPosition = car[*indexCar].GetHistory(carTracking);

		if (!carVerified){
			/* exit section [car]*/
			carWriteCount[*indexCar]--;
			pthread_mutex_unlock(&mtx_wrtCar[*indexCar]);
			once = 1;
			continue;
		}


		pthread_mutex_lock(&mtx_readFrame);
		frameReadCount++;
		if (frameReadCount == 1)
			pthread_mutex_lock(&mtx_wrtFrame);
		pthread_mutex_unlock(&mtx_readFrame);

		if (frame.empty()){
			cout << "< frame is empty >" << endl;
			/* exit section [frame] */
			pthread_mutex_lock(&mtx_readFrame);
			frameReadCount--;
			if (frameReadCount == 0)
				pthread_mutex_unlock(&mtx_wrtFrame);
			pthread_mutex_unlock(&mtx_readFrame);

			/* exit section [car] */
			carWriteCount[*indexCar]--;
			pthread_mutex_unlock(&mtx_wrtCar[*indexCar]);
			continue;
		}

		frame.copyTo(image);
		cvtColor(frame, gray, COLOR_BGR2GRAY);






		if (once)
		{
			cout << "< goodFeaturesToTrack Start >" << endl;


			rod = image(carCurrPosition);
			cvtColor(rod, rodGray, COLOR_BGR2GRAY);

			goodFeaturesToTrack(rodGray, points[0], MAX_COUNT, 0.02, 10, Mat(), 3, 0, 0.04);
			cornerSubPix(rodGray, points[0], subPixWinSize, cvSize(-1, -1), termcrit);
			for (int i = 0; i < points[0].size(); i++) {
				points[0][i].x += carCurrPosition.x;
				points[0][i].y += carCurrPosition.y;
			}

			once = 0;
			gray.copyTo(prevGray);
			/* exit section [frame] */
			pthread_mutex_lock(&mtx_readFrame);
			frameReadCount--;
			if (frameReadCount == 0)
				pthread_mutex_unlock(&mtx_wrtFrame);
			pthread_mutex_unlock(&mtx_readFrame);

			/* exit section [car] */
			carWriteCount[*indexCar]--;
			pthread_mutex_unlock(&mtx_wrtCar[*indexCar]);
			continue;
		}
		else if (!points[0].empty()){
			vector<uchar> status;
			vector<float> err;
			if (prevGray.empty())
				gray.copyTo(prevGray);



			//cout << "calcOpticalFlowPytLK START" << endl;
			calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 1, 0.001);
			size_t i, k;
			int avg_x = 0;
			int avg_y = 0;

			/*
			for (i = k = 0; i < points[1].size(); i++)
			{
			//if (i < points[1].size() - 1){
			//	if (norm(points[1][i + 1] - points[1][i]) <= 5)
			//		continue;
			//}
			//if (!status[i])
			//	continue;
			if (points[1][i].x > image.size().width)
			continue;
			points[1][k++] = points[1][i];

			circle(image, points[1][i], 4, Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)), -1, 8, 0);
			}

			points[1].resize(k);

			if (points[1].size() < 2) {
			points[1].clear();
			k = 0;
			}
			*/

			//	cout << points[1].size() << " ";
			if (1/*k != 0*/){
				Rect newPosition = boundingRect(points[1]);
				double ratio = 0.0;
				ratio = ((double)newPosition.width / (double)newPosition.height);

				car[*indexCar].UpdateHistory(newPosition);
				/*	if (ratio > 0.2 && ratio < 2.5)
				car[*indexCar].UpdateHistory(newPosition);
				else
				{
				if (car[*indexCar].GetPenalty() < MAXPENALTY)
				car[*indexCar].UpdatePenalty();
				else {
				car[*indexCar].Die();
				cout << "car[" << *indexCar << "] is Die\n";
				}
				}*/
			}
			// Stop tracking
			else{


				if (car[*indexCar].GetPenalty() < MAXPENALTY)
					car[*indexCar].UpdatePenalty();
				else {
					car[*indexCar].Die();
					cout << "car[" << *indexCar << "] is Die\n";
				}

			}
		}


		/* exit section [frame] */
		pthread_mutex_lock(&mtx_readFrame);
		frameReadCount--;
		if (frameReadCount == 0)
			pthread_mutex_unlock(&mtx_wrtFrame);
		pthread_mutex_unlock(&mtx_readFrame);


		/* exit section [car]*/
		carWriteCount[*indexCar]--;
		pthread_mutex_unlock(&mtx_wrtCar[*indexCar]);
		//imshow(tracker, image);

		char c = (char)waitKey(10);
		if (c == 27)
			break;

		std::swap(points[1], points[0]);
		cv::swap(prevGray, gray);
	}

	return 0;
}



int main(){

	pthread_t *pThreads;
	pThreads = (pthread_t *)malloc(sizeof(pthread_t) * MAX_THREADS);
	//initialize mutex and wrt
	for (int i = 0; i < MAXTARGET; i++){
		pthread_mutex_init(&mtx_readCar[i], NULL);
		pthread_mutex_init(&mtx_wrtCar[i], NULL);
	}
	pthread_mutex_init(&mtx_readFrame, NULL);
	pthread_mutex_init(&mtx_wrtFrame, NULL);

	String VideoName = "testavi2.mp4";
	String CascadeName1 = "./YK_F.xml";
	String CascadeName2 = "./YK_FL.xml";
	String CascadeName3 = "./YK_RL.xml";

	threadInfo.vc = new VideoCapture(VideoName);
	//VideoCapture *vc = new VideoCapture(VideoName);
	TermCriteria termcrit(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03);
	Size subPixWinSize(10, 10), winSize(31, 31);

	bool thOnce = true;
	const int MAX_COUNT = 500;

	if (!threadInfo.vc->isOpened())
	{
		cout << "Could not initialize capturing...\n";
		return 0;
	}

	__int64 freq, start, finish;
	::QueryPerformanceFrequency((_LARGE_INTEGER*)&freq);

	int zero = 0;
	int one = 1;
	int two = 2;
	pthread_create(&pThreads[0], NULL, OpticalFlowThread, &zero);
	pthread_create(&pThreads[1], NULL, OpticalFlowThread, &one);
	pthread_create(&pThreads[2], NULL, OpticalFlowThread, &two);


	while (1){

		/********************************************/
		/****************** WRITE *******************/
		/************ resource : frame **************/
		/********************************************/

		/* entry section */
		pthread_mutex_lock(&mtx_wrtFrame);
		frameWriteCount++;
		if (frameWriteCount > 1){
			cout << "err : this is occupied by another writer\n";
			//	while (writeCount > 1);
		}

		/* critical section */
		*threadInfo.vc >> frame;



		/* critical section */
		if (frame.empty())	continue;
		frame.copyTo(dFrame);





		if (thIdx > MAX_THREADS){
			cout << "THREAD is Full\n";
			//free(pThreads);
			//system("pause"); // for DEBUG
		}

		::QueryPerformanceCounter((_LARGE_INTEGER*)&start);

		//Detect_haarcascades(vc);
		threadInfo.detectedRoi.clear();
		Detect_haarcascades2(dFrame, threadInfo.detectedRoi, CascadeName1, CascadeName2, CascadeName3);



		/* exit section */
		frameWriteCount--;
		pthread_mutex_unlock(&mtx_wrtFrame);
		compare(threadInfo.detectedRoi);


		// processing time (fps)
		::QueryPerformanceCounter((_LARGE_INTEGER*)&finish);
		double fps = freq / double(finish - start + 1);
		char fps_str[20];
		sprintf_s(fps_str, 20, "FPS: %.1lf", fps);
		putText(dFrame, fps_str, Point(5, 35), FONT_HERSHEY_SIMPLEX, 1., Scalar(0, 255, 0), 2);

		char car_str[20];
		for (int k = 0; k < MAXTARGET; k++){
			pthread_mutex_lock(&mtx_readCar[k]);
			carReadCount[k]++;
			if (carReadCount[k] == 1)
				pthread_mutex_lock(&mtx_wrtCar[k]);
			pthread_mutex_unlock(&mtx_readCar[k]);

			/* critical section */
			if (car[k].GetVerified()){
				sprintf_s(car_str, 20, "Car %d", k);
				rectangle(dFrame, car[k].GetHistory(car[k].GetTracking()), Scalar(10, 255, 100), 3);
				putText(dFrame, car_str, Point(car[k].GetHistory(car[k].GetTracking()).x, car[k].GetHistory(car[k].GetTracking()).y),
					FONT_HERSHEY_SIMPLEX, 1., Scalar(255, 255, 255), 1);
			}

			pthread_mutex_unlock(&mtx_readCar[k]);
			carReadCount[k]--;
			if (carReadCount[k] == 0)
				pthread_mutex_unlock(&mtx_wrtCar[k]);
			pthread_mutex_unlock(&mtx_readCar[k]);
		}
		imshow("avengers", dFrame);

		char ch = waitKey(10);
		if (ch == 27) break;            // ESC Key
		else if (ch == 32)               // SPACE Key
		{
			while ((ch = waitKey(10)) != 32 && ch != 27);
			if (ch == 27) break;
		}
	}

	/* waiting for threads */
	for (int i = 0; i < MAX_THREADS; i++)
		pthread_join(pThreads[i], NULL);

	free(pThreads);
	pthread_mutex_destroy(&mtx_readFrame);
	pthread_mutex_destroy(&mtx_wrtFrame);
	pthread_mutex_destroy(mtx_readCar);
	pthread_mutex_destroy(mtx_wrtCar);


	//WaitForMultipleObjects(thIdx, tHandles, TRUE, INFINITE);


	//
	//for (DWORD i = 0; i < thIdx; i++){
	//	CloseHandle(tHandles[i]);
	//}

}

#if 1
int compare(vector<Rect> rect_in)
{


	int count;
	int x, xx, y, yy = 0;
	int que[NUMRECT] = { NUMRECT, };
	int numCandy = rect_in.size();
	int S1, S2, S3;
	double ratio;

	//Check valid cars
	for (int a = 0; a < rect_in.size(); a++)
	{
		//initializing
		count = 0;
		for (int k = 0; k < NUMRECT; k++)
			que[k] = NUMRECT;

		/********************************************/
		/************ resource : car[] **************/
		/********************************************/


		// READ ONLY 
		for (int b = 0; b < MAXTARGET; b++) {

			/* entry section */
			pthread_mutex_lock(&mtx_readCar[b]);
			carReadCount[b]++;
			if (carReadCount[b] == 1){
				pthread_mutex_lock(&mtx_wrtCar[b]);
			}
			if (carWriteCount[b] != 0){
				cout << "err : writecount is not zero\n";
				//	while (writeCount != 0);
			}
			pthread_mutex_unlock(&mtx_readCar[b]);


			/* critical section */
			if (car[b].GetVerified())
			{
				if (!((rect_in[a].y + rect_in[a].height < car[b].GetHistory(car[b].GetTracking()).y) ||
					(rect_in[a].y > car[b].GetHistory(car[b].GetTracking()).y + car[b].GetHistory(car[b].GetTracking()).height) ||
					(rect_in[a].x > car[b].GetHistory(car[b].GetTracking()).x + car[b].GetHistory(car[b].GetTracking()).width) ||
					(rect_in[a].x + rect_in[a].width <car[b].GetHistory(car[b].GetTracking()).x)))// !(안겹치는 조건) is 겹치는 조건
				{

					//car[b].GetTracking() : last history index
					x = (rect_in[a].x > car[b].GetHistory(car[b].GetTracking()).x) ? rect_in[a].x : car[b].GetHistory(car[b].GetTracking()).x;
					xx = (rect_in[a].x + rect_in[a].width > car[b].GetHistory(car[b].GetTracking()).x + car[b].GetHistory(car[b].GetTracking()).width) ? car[b].GetHistory(car[b].GetTracking()).x + car[b].GetHistory(car[b].GetTracking()).width : rect_in[a].x + rect_in[a].width;
					y = (rect_in[a].y > car[b].GetHistory(car[b].GetTracking()).y) ? car[b].GetHistory(car[b].GetTracking()).y : rect_in[a].y;
					yy = (rect_in[a].y + rect_in[a].height > car[b].GetHistory(car[b].GetTracking()).y + car[b].GetHistory(car[b].GetTracking()).height) ? car[b].GetHistory(car[b].GetTracking()).y + car[b].GetHistory(car[b].GetTracking()).height : rect_in[a].y + rect_in[a].height;
					S1 = 0;
					S2 = 0;
					S3 = 0;
					ratio = 0.0;
					if (xx > x && yy > y)
					{
						S1 = rect_in[a].width * rect_in[a].height;
						S2 = car[b].GetHistory(car[b].GetTracking()).width * car[b].GetHistory(car[b].GetTracking()).height;
						S3 = (xx - x) * (yy - y);
						ratio = (double)(S2 + S1)/(double)max(S2, S1)* (double)S3 / (double)max(S1, S2);
					}

					if (ratio > 0.5)
					{
						que[count] = b;//겹치는 Rect를 찾아서 que에 그 이름을 저장
						count++;
					}
				}
			}

			/* exit section */
			pthread_mutex_lock(&mtx_readCar[b]);
			carReadCount[b]--;
			if (carReadCount[b] == 0){
				pthread_mutex_unlock(&mtx_wrtCar[b]);
			}
			pthread_mutex_unlock(&mtx_readCar[b]);
			/******************************************/
		}


		/********************************************/
		/****************** WRITE *******************/
		/************ resource : car[] **************/
		/********************************************/


		/* critical section */
		if (count == 0) // THIS IS A NEW CAR
		{
			cout << "This is a new Car" << endl;
			int k = 0;
			for (k = 0; k < MAXTARGET; k++)
			{
				/* entry section */
				pthread_mutex_lock(&mtx_wrtCar[k]);

				//wrt.lock();
				carWriteCount[k]++;
				if (carWriteCount[k] > 1) {
					cout << "err : this is occupied by another writer\n";
					//	while (writeCount > 1);
				}


				/* critical section */
				if (!car[k].GetVerified()) {

					car[k].UpdateVerified(true);
					car[k].UpdateNew(rect_in[a]);


					/* exit section */
					carWriteCount[k]--;
					pthread_mutex_unlock(&mtx_wrtCar[k]);
					break;

				}

				/* exit section */
				carWriteCount[k]--;
				pthread_mutex_unlock(&mtx_wrtCar[k]);

			}

		}

		else
		{
			// SEWON


			for (int k = 0; k < count; k++)//que[k] is the index of same car
			{
				/* entry section */
				pthread_mutex_lock(&mtx_wrtCar[que[k]]);
				carWriteCount[que[k]]++;
				if (carWriteCount[que[k]] > 1){
					cout << "err : this is occupied by another writer\n";
					//	while (writeCount > 1);
				}

				/* critical section */
				car[que[k]].UpdateHistory(0, car[que[k]].GetHistory(car[que[k]].GetTracking()));
				car[que[k]].RenewTracking(0);

				/* exit section */
				carWriteCount[que[k]]--;
				pthread_mutex_unlock(&mtx_wrtCar[que[k]]);
			}
		}
	}

	//wrt.lock();

	//SEWON

	//Check invalid cars
	for (int b = 0; b < MAXTARGET; b++) {
		/* entry section */
		pthread_mutex_lock(&mtx_wrtCar[b]);
		carWriteCount[b]++;
		if (carWriteCount[b] > 1){
			cout << "err : this is occupied by another writer\n";
			//	while (writeCount > 1);
		}

		if (car[b].GetVerified())
		{
			if (car[b].GetTracking() > MAX_TRACK){
				if (car[b].GetPenalty() < MAXPENALTY)
					car[b].UpdatePenalty();
				else{
					car[b].Die();

					cout << "car[" << b << "] is Die\n";
				}
			}

		}
		/* exit section */
		carWriteCount[b]--;
		pthread_mutex_unlock(&mtx_wrtCar[b]);
		/******************************************/

	}
	return 0;
}

#endif