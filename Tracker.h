#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "use_opencv.h"



struct CallbackParam
{
	Mat frame;
	Point pt1, pt2;
	Rect roi;
	bool drag;
	bool updated;
};

struct ThreadInfo{
	VideoCapture *vc;
	vector<Rect> detectedRoi[3];
	int indexDetectedRoi;
};


int BSD_OpticalFlowLK(VideoCapture *vc, vector<Rect> detectedRoi);
//int OpticalFlowThread2(void* arg);
int OpticalFlowThread2(ThreadInfo *arg);
unsigned int __stdcall OpticalFlowThread(void* arg);
//void* OpticalFlowThread(void* arg){