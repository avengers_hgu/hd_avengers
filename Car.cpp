#include "Car.h"
#include <stdio.h>

Car::Car() {
	//m_aHistory = NULL;
	m_Tracking = 0;
	m_Penalty = 0;
	m_Verified = 0;
}

/*
Car::Car(int nHistory) {
m_aHistory = NULL;
m_Tracking = 0;
m_Penalty = 0;
m_Verified = 0;
Alloc(nHistory);
}
*/

/*
int Car::Alloc(int nHistory) {
if (IsAllocated())
Delete();

//m_aHistory = new Rect[nHistory];
if (m_aHistory == NULL) {
printf("Failed to allocate memory in %s (%s %d)\n", __FUNCTION__, __FILE__, __LINE__);
return FALSE;
}

return TRUE;
}
*/
//deallocate memory
/*
void Car::Delete() {
if (m_aHistory) {
delete[] m_aHistory;
m_aHistory = NULL;
}

m_Tracking = 0;

}
*/

// m_Tracking > MAX_TRACK ? m_Tracking = 0 : m_Tracking++ 
void Car::UpdateTracking() {
	m_Tracking++;
}

// m_Tracking = index 
void Car::RenewTracking(int index) {
	if (index < MAX_TRACK){
		m_Tracking = index;
	}
}

//clear tracking history : m_Tracking -> 0
//overite m_aHistory
//increase m_Penalty if penalty is HIGH

// m_Penalty++, m_aHistory[0] = m_aHistory[nTracking], m_Tracking = 0 
void Car::UpdatePenalty() {
	m_Penalty++;
	m_aHistory[0] = m_aHistory[m_Tracking];
	m_Tracking = 0;
}

// m_aHistory[m_Tracking] = newCar 
void Car::UpdateNew(Rect newCar){
	m_Tracking = 0;
	m_aHistory[m_Tracking] = newCar;
}

// m_aHistory[m_Tracking] = newCar 
void Car::UpdateHistory(Rect newCar){
	if (m_Tracking < MAXHIST - 1)
	{
		UpdateTracking();
		m_aHistory[m_Tracking] = newCar;
	}
	else
	{
		m_aHistory[m_Tracking] = newCar;
	}
}


// m_aHistory[index] = newCar 
void Car::UpdateHistory(int index, Rect newCar){
	if (index < MAXHIST){
		m_aHistory[index] = newCar;
	}
	else
		printf("%d is out of range m_aHistory[]\n", index);
}


// m_Verified = True or False
void Car::UpdateVerified(bool tf){
	m_Verified = tf;
}


void Car::Die(){
	m_Tracking = 0;
	m_Verified = false;
	m_Penalty = 0;
}
